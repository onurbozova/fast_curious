#!/bin/sh

rm -rf ~/catkin_ws a1_456* ~/.gazebo/models/*
cp -R catkin_ws ~/
cd models/
cp -R * ~/.gazebo/models/
cd ..
#sed -i "s/kosantosbik/$USER/g" ~/catkin_ws/src/a1_456_referee/worlds/shop1.world
source ~/catkin_ws/devel/setup.bash
cd ~/catkin_ws
catkin_make
